#!/usr/bin/perl -w 

# Ariel Rios 
# ariel@gnu.org 
# Script for autogenerating automake.am files
# August 12, 2k2
# Released under the GNU GPL

use Fcntl ':mode';

@FILE_EXTENSION = (".class", ".inc", ".php", ".mdl", "g.in");
#$AMNAME= "Makefile.am";

$MODNAME = "data";
$MODREALNAME = $ARGV[0];
$MYPREFIX = "\$(prefix)/lib/php/servicios/serv-datos";
$SCRIPTS = $ARGV[1];
#$OTHERFILE = "$SCRIPTS.in";

sub help {
  print "Script Generador de Makefile.am\n";
  print 'Ariel Rios <jrios@elektra.com.mx>' . "\n";
  print "Uso: automake-am.sh [nombre-del-modulo] [algun-script]\n";
}

sub getrealname{
    
    if (length ($_[0]) ge 2){
	return substr ($_[0], 2, length($_[0]));
    }
	return " ";
}

sub comment {
    "# Makefile.am autogenerado para $MODREALNAME
#
# Author: Ariel Rios <jrios\@elektra.com.mx>\n\n";
    }


sub path {
    my($name) =  &getrealname($_[0]);
    "$MODNAME = \$(prefix)$MYPREFIX/$name\n\n";
}

sub binscripts {
    "bin_SCRIPTS = $SCRIPTS\n\n";
}



sub getdirs{
    opendir (DIR, $_[0]) || die "No existe el tal directorio"; 

    my(@files) = readdir(DIR);
    my(@subd);
    #print "ariel @files";
    foreach $filename (@files){
	my($mode) = (stat("$_[0]/$filename"))[2];	
	if (S_ISDIR($mode) && $filename ne "CVS" and $filename ne "." 
	    && $filename ne ".." && $filename ne "macros"){
	 
	    $subd[$#subd+1] = $filename; 
	    
	}
	
    }
    closedir DIR;

    return @subd;

}



sub loadfiles {

    opendir (DIR, $_[0]) || die "No se pudo abrir el $_[0]en ";
    
    my(@files) = readdir (DIR);
    my (@modules)=();
    
    foreach $file (@files){
	$_ = $file;
	foreach $ext(@FILE_EXTENSION){
	    if (/\d*$ext/){ #FIXME
		$modules[$#modules+1] = $_;
	    }
	}
	closedir DIR;
    }
    @modules;
}


sub datalocal{
    my(@files) =  &loadfiles($_[0]);
    my($name) = &getrealname($_[0]);
    my($str) = "install-data-local:\n\t\$(mkinstalldirs) \$($MODNAME)\n";
    foreach $file (@files){
	$str .= "\t\$(INSTALL_DATA) \$(srcdir)/$file \$($MODNAME)/$file \n";
    }
    $str; 
}        


sub extradist{   
    my(@files) =  &loadfiles($_[0]);
    if ($#files ge 1){
	my($str) = "\nEXTRA_DIST = ";
	foreach $file (@files){
	    $str .= "$file \\\n";
	}
	return substr ($str,  0, rindex($str, "\\\n")) . "\n";
    }
    " ";
}


sub subdirs {
    my(@dirs) = &getdirs($_[0]);
    if ($#dirs ge 1){
	my($str) = "\nSUBDIRS = ";
	foreach $dir (@dirs){
	    $str .=  "$dir \\\n";
	}
	return substr ($str,  0, rindex($str, "\\\n")) . "\n";
    }
    " ";
}




sub doall {

    my($LOCALDIR) = $_[0];
    my($MAKEFILE) = "$LOCALDIR/Makefile.am";
    
    die unless open (FILE,">>$MAKEFILE");

    print FILE &comment();
    print FILE &path($LOCALDIR);
    
    if ($_[1]){
	print FILE &binscripts();
    }
    
    print FILE &datalocal($LOCALDIR);
    print FILE &extradist($_[0]);
    print FILE &subdirs($LOCALDIR);
    my(@SUBD) = &getdirs ($LOCALDIR);
    close FILE;
    foreach $dir (@SUBD){
	my($dd) = "$LOCALDIR/$dir";
	#print FILE "ahora voy a $dd\n\n";
	&doall("$dd", 0);
    }



}

if ($ARGV[0] and $ARGV[1]){
    &doall(".", 1);
}

else { 
    help();
}

