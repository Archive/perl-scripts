#!/usr/bin/perl -w

# Ariel Rios 
# ariel@gnu.org
# CVS commit script
# September 19, 2k1
# Released under the GNU GPL

open (FILE, "./ChangeLog") || die "ChangeLog non est!\n";

read (FILE, $str, 1024);

@array = split (/\n\d{4}/, $str, 2);

close (FILE);

$command = "cvs commit -m \"$array[0] \"";

system ($command);
